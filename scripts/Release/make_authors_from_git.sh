#!/bin/bash

AUTHORS="./AUTHORS.md"
if ! [ -f "$AUTHORS" ]
then
    AUTHORS="../AUTHORS.md"
    if ! [ -f "$AUTHORS" ]
    then
	echo "no authors file found, exiting"
	exit
    fi
fi

# order by number of commits
# git shortlog -e -s -n | \
git log --format='%ae' | \
    grep -v metalab | \
    grep -v 4d3d3d3 | \
    sort | \
    sed 's/eodelorme@sat.qc.ca/Émile Ouellet-Delorme/' | \
    sed 's/jwantz@sat.qc.ca/Julien Wantz/' | \
    sed 's/marie-eve.dumas@mail.mcgill.ca/Marie-Eve Dumas/' | \
    sed 's/hvenkatesan@sat.qc.ca/Harish Venkatesan/' | \
    sed 's/gdowns@sat.qc.ca/Gabriel Downs/' | \
    sed 's/gndowns@gmail.com/Gabriel Downs/' | \
    sed 's/edurand@sat.qc.ca/Emmanuel Durand/' | \
    sed 's/emmanueldurand@gmail.com/Emmanuel Durand/' | \
    sed 's/nbouillot@sat.qc.ca/Nicolas Bouillot/' | \
    sed 's/nicolas.bouillot@gmail.com/Nicolas Bouillot/' | \
    sed 's/djiamnot@gmail.com/Michal Seta/' | \
    uniq -c | \
    sort -bgr | \
    sed 's/\ *[0-9]*\ /\* /' > ${AUTHORS}

