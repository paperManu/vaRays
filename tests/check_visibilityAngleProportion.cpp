#undef NDEBUG // get assert in release mode

#include "../varays/Context/RayTracer/utils.hpp"
#include "./testUtils.hpp"

using namespace varays;

int main()
{
    for (int i = 0; i < 1000; ++i)
    {
        float proportion = utils::visibilityAngleProportion(testUtils::unitRandFloat() * 5, testUtils::randVec3(), testUtils::randVec3());
        assert(proportion < 1.f && proportion > 0.f);
    }
    return 0;
}