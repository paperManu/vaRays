#undef NDEBUG // get assert in release mode

#include <Eigen/Dense>

#include "../varays/Context/RayTracer/utils.hpp"
#include "./testUtils.hpp"

using namespace varays;

int main()
{
    Eigen::Vector3f origin = Eigen::Vector3f(0.0, 0.0, 0.0);
    for (int i = 0; i < 1000; ++i)
    {   
        Eigen::Vector3f raydir = utils::sphereRandom().normalized();
        Eigen::Vector3f normal = utils::sphereRandom().normalized();
        // Make sure the normal and incoming ray directions are within 180* of each other
        if (raydir.dot(normal) > 0)
            normal = Eigen::Vector3f(-normal[0], -normal[1], -normal[2]);
        int nbrays = rand() % 9 + 1; // random int between 1 and 10
        std::vector<utils::RayToThrow> raysToThrow = utils::reflectionDirections(origin, raydir, normal, nbrays);
        for (const auto& ray : raysToThrow)
        {
            assert(ray.weights.specular <= 1.0);
            assert(ray.weights.diffuse <= 1.0);
        }
    }

    return 0;
}