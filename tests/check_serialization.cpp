#undef NDEBUG // get assert in release mode

#include <cassert>
#include <fstream>

#include <Eigen/Dense>

#include "varays/Common/SoundEvent.hpp"
#include "varays/Context/RayTracer/utils.hpp"
#include "varays/Context/VaraysContext.hpp"
#include "varays/Utils/JsonSave.hpp"

using namespace varays;

struct Params // Default parameters for ray tracing
{
    VaraysContext* context_;
    int ambiOrder_ = 0;
    int initRays_ = 500; // Amount of "original" rays shot from the user location
    int nbRay_ = 3; // Amount of reflected rays for every reflection on a surface
    int rvrb_ = 3; // Maximum amount of reflections
    int splitReverb_ = 3; // After this amount of reflections, all reflections will only be specular
    float minEnergyThreshold_ = 0.005; // Threshold of energy (fraction of its initial ray) a ray must have in order to be thrown
    bool cache_ = false;
    bool diffraction_ = false;
    float attenuationPower_ = 2; // Power of the attenuation factor with distance (No attenuation = 0, Real world attenuation = 2)
};

int main()
{
    Params prmtr;

    Eigen::Vector3f listPos(-.5f, 1.0f, -1.5f);

    VaraysContext context("", listPos, prmtr.cache_);
    prmtr.context_ = &context;

    context.readObjFile("./check_cubeRoom.obj", prmtr.diffraction_);

    // Add a new source at (0, 0, 0)
    prmtr.context_->addSource({0.0f, 0.0f, 0.0f}, "newSource");

    context.addRayManager(0, 4, 1, 0.005);
    std::vector<SoundEvent> soundEvents = context.executeRayManagers(100, 4, 3);

    assert(!soundEvents.empty()); // this vector cannot be empty for the given test case of a cube room

    // Serialize sndEvnts into a string
    std::string soundEvents_jsonString;
    jsonSerialize<SoundEvent>(soundEvents, soundEvents_jsonString);
    assert(!soundEvents_jsonString.empty()); // check if the vector has been serialized

    // Write to JSON file
    std::ofstream ofile;
    ofile.open("./SoundEvents_serializationTest.json");
    ofile << soundEvents_jsonString;
    ofile.close();

    // Read from JSON file
    std::ifstream ifile;
    ifile.open("./SoundEvents_serializationTest.json");
    std::string soundEvents_fromJsonFile((std::istreambuf_iterator<char>(ifile)), (std::istreambuf_iterator<char>()));
    ifile.close();
    assert(!soundEvents_fromJsonFile.empty()); // check if the json file was read properly

    // Deserialize info into vector of SoundEvents
    std::vector<SoundEvent> soundEvents_deserialized;
    jsonDeserialize<SoundEvent>(soundEvents_deserialized, soundEvents_fromJsonFile);
    assert(soundEvents_deserialized.empty() == false); // check if the data was deserialized correctly

    // check if the two vectors are the same (This also checks if the two vectors are of the same size)
    assert(soundEvents == soundEvents_deserialized);
}
