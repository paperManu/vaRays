//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

/* In this test, we first generate an impulse response containing random
 * samples and pass it to the convolver. We then pass an impulse signal
 * to the convolver and compare its output against the IR. If everything
 * works well, the output of the convolver must be the same as the IR itself.
 */

#undef NDEBUG // get assert in release mode

#include <cassert>
#include <cmath>
#include <stdlib.h>
#include <time.h>

#include "../varays/Auralizer/VaraysAuralizer.hpp"

int main()
{
    VaraysAuralizer convolver;
    assert(convolver.isAllocated());

    VaraysAuralizer convolver_vectorized;
    assert(convolver_vectorized.isAllocated());

    convolver_vectorized.setVectorizedOperations(true); // Enable SIMD vectorized MAC operations in the convolver

    // create a new source
    convolver.newSource("ConvTest");
    convolver_vectorized.newSource("ConvTest");

    unsigned int sampRate = convolver.getJclient()->fsamp();
    unsigned int jackBufSize = convolver.getJclient()->fragm();

    srand(time(nullptr));
    for (int trial = 0; trial < 10; ++trial)
    {
        unsigned int irLen = (rand() % (2 * sampRate / jackBufSize - 1) + 1) * jackBufSize; // ~2 second long impulse response
        std::vector<float> ir(irLen);
        float randmaxInv = 1.0f / static_cast<float>(RAND_MAX);
        for (unsigned int i = 0; i < irLen; ++i)
            ir[i] = rand() * randmaxInv * 2.0f - 1.0f;
        convolver.updateIR("ConvTest", 1, ir);
        convolver_vectorized.updateIR("ConvTest", 1, ir);

        std::vector<float> jackInpBuf(jackBufSize, 0);
        std::vector<float> output(irLen);
        std::vector<float> output_vectorized(irLen);

        assert(output.data());
        assert(output_vectorized.data());
        // Let the convolver run a few cycles
        for (int i = 0; i < 100; ++i)
        {
            convolver.getJclient()->jack_process(&jackInpBuf[0], &output[0]);
            convolver_vectorized.getJclient()->jack_process(&jackInpBuf[0], &output_vectorized[0]);
        }
        std::fill(output.begin(), output.end(), 0);
        std::fill(output_vectorized.begin(), output_vectorized.end(), 0);

        // capture output
        jackInpBuf[0] = 1;
        float floatDiff = 0;
        float floatDiff_vectorized = 0;
        for (unsigned int sampC = 0; sampC < irLen; sampC += jackBufSize)
        {
            convolver.getJclient()->jack_process(&jackInpBuf[0], &output[sampC]);
            convolver_vectorized.getJclient()->jack_process(&jackInpBuf[0], &output_vectorized[sampC]);
            for (unsigned int i = 0; i < jackBufSize; ++i)
            {
                floatDiff += std::abs(ir[i] - output[i]);
                floatDiff_vectorized += std::abs(ir[i] - output_vectorized[i]);
            }

            if (sampC == 0)
                jackInpBuf[0] = 0;
        }
        assert(floatDiff < __FLT_EPSILON__ * irLen);
        assert(floatDiff_vectorized < __FLT_EPSILON__ * irLen);
    }
    return 0;
}
