// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the
// Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
// Boston, MA 02110-1301, USA.

#include <cstdlib>
#include <Eigen/Dense>
#include <random>
#include <vector>

namespace testUtils
{

 /**
 * Generate a random float between 0.f and 1.f
 * @return Return a random float
 */
float unitRandFloat() // random float between 0 and 1
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0);
    return dis(gen);
}

 /**
 * Generate a random Eigen::Vector3f  with 3 values between -10.f and 10.f
 * @return Return a random Eigen::Vector3f
 */
Eigen::Vector3f randVec3() // vec3 with 3 random values between -10 and 10
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-10.0, 10.0);
    return Eigen::Vector3f(dis(gen), dis(gen), dis(gen));
}
}