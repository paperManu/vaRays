These are C++ programs performing timing tests for both the techniques, [delayed FIR method](./Python/FIR_approach/OctaveBandFIRFilters.ipynb) and the [IIR LR Crossover filter method](./Python/IIR_approach/LinkwitzRileyFilterTree.ipynb). Both require Eigen3 to be installed. 

The IIR method is a better option for the following reasons:

- **Scalable**: It does not depend on the number of paths. We can increase the resolution of the room impulse response by throwing more rays without a significant computational cost increase. In the FIR method, we basically copy the time domain filter  kernels for each path, hence the cost is proportional to the number of paths that exist between the listener and the source.
- **Allows interpolation of paths between samples** - We may be able to get more accurate room impulse responses by using interpolation techniques for paths arriving at fractional delays ,i.e., a path arrives between two sampling instances of the IR. For similar results using the FIR method, we may have to interpolate each sample in all filter kernels (for 8 octave bands, we need a minimum of 512 samples per kernel) which will drastically increase the cost of computation.
- **Better frequency resolution** - The frequency response of the resulting IRs will be smoother since we use recursive filters rather that finite impulse responses. Using FIR filters will limit the frequency resolution of the resulting room impulse responses. We may have to perform listening tests to actually tell if the limited frequency resolution is perceivable, but we can expect the IIR method will work better than the FIR method regardless.

The following table summarizes timing tests that were run on my machine with an i5-4thGen processor and 8GB DDR3 RAM and -O3 level compiler optimization. For the IIR LR crossover filter method, the timing results of the [horizontal approach](./iirFilterEigenTest_hor.cpp) are report, which is 2x faster than the [vertical approach](./iirFilterEigenTest_ver.cpp).

<table>
<thead>
  <tr>
    <th rowspan="3">Technique</th>
    <th rowspan="3">IR Length (samples)</th>
    <th colspan="4">Time / IR (ms)</th>
  </tr>
  <tr>
    <td colspan="4"><span style="font-weight:bold">Number of Paths</span></td>
  </tr>
  <tr>
    <td>10K</td>
    <td>25K</td>
    <td>50k</td>
    <td>100K</td>
  </tr>
</thead>
<tbody>
  <tr>
    <td rowspan="3">Delayed FIR</td>
    <td>24000</td>
    <td>5.9</td>
    <td>10.2</td>
    <td>21.3</td>
    <td>41.3</td>
  </tr>
  <tr>
    <td>48000</td>
    <td>4.8</td>
    <td>10.9</td>
    <td>21.4</td>
    <td>42.5</td>
  </tr>
  <tr>
    <td>96000</td>
    <td>4</td>
    <td>10.7</td>
    <td>21.6</td>
    <td>44.7</td>
  </tr>
  <tr>
    <td rowspan="3">IIR LR Crossover</td>
    <td>24000</td>
    <td>6</td>
    <td>5.8</td>
    <td>6.2</td>
    <td>6.1</td>
  </tr>
  <tr>
    <td>48000</td>
    <td>11.3</td>
    <td>11.3</td>
    <td>11.4</td>
    <td>12</td>
  </tr>
  <tr>
    <td>96000</td>
    <td>23.6</td>
    <td>23.4</td>
    <td>23.7</td>
    <td>24.8</td>
  </tr>
</tbody>
</table>