#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>

static const unsigned int numtrials = 10;

using mytype = float;
using ChronoResolution = std::chrono::milliseconds;

template <typename T> class SOS
{
  private:
    T b0_, b1_, b2_;
    T a0_, a1_, a2_;
    T s1_, s2_;

  public:
    SOS(const std::vector<T> coeffs)
    {
        if (coeffs.size() != 6)
        {
            return;
        }
        s1_ = 0;
        s2_ = 0;

        b0_ = coeffs[0];
        b1_ = coeffs[1];
        b2_ = coeffs[2];
        a0_ = coeffs[3];
        a1_ = coeffs[4];
        a2_ = coeffs[5];
    }

    void process(T& inSamp)
    {
        T temp = inSamp;
        inSamp = inSamp * b0_ + s1_;
        s1_ = temp * b1_ - inSamp * a1_ + s2_;
        s2_ = temp * b2_ - inSamp * a2_;
    }

    void processBlock(std::vector<T>& samp)
    {
        for (auto& s : samp)
            process(s);
    }

    void clearStates() { s1_ = s2_ = 0; }
};

template <typename T> class SOS_Cascade
{
  private:
    std::vector<SOS<T>> sosCasc_;

  public:
    SOS_Cascade(const std::vector<std::vector<T>> coeffs)
    {
        for (auto c : coeffs)
        {
            SOS<T> temp(c);
            sosCasc_.push_back(temp);
        }
    }

    T process(T inSamp)
    {
        T out = inSamp;
        for (auto& sos : sosCasc_)
            out = sos.process(out);
        return out;
    }

    void processBlock(std::vector<T>& samp)
    {
        for (auto& sos : sosCasc_)
            sos.processBlock(samp);
    }

    void clearStates()
    {
        for (auto& sos : sosCasc_)
            sos.clearStates();
    }
};

int main()
{
    int fs = 48000;

    std::vector<SOS_Cascade<mytype>> stage0;
    stage0.push_back(
        SOS_Cascade<mytype>({{1.73460199e-08, 3.46920399e-08, 1.73460199e-08, 1.00000000e+00, 0.00000000e+00, 0.00000000e+00}, {1., -2., 1., 1., 0., 0.}}));
    stage0.push_back(SOS_Cascade<mytype>({{0.96780473, -1.93560945, 0.96780473, 1., 0., 0.}, {1., -2., 1., 1., 0., 0.}}));
    stage0.push_back(
        SOS_Cascade<mytype>({{4.03612414e-06, 8.07224827e-06, 4.03612414e-06, 1.00000000e+00, 0.00000000e+00, 0.00000000e+00}, {1., -2., 1., 1., 0., 0.}}));
    stage0.push_back(SOS_Cascade<mytype>({{0.87730194, -1.75460389, 0.87730194, 1., 0., 0.}, {1., -2., 1., 1., 0., 0.}}));
    stage0.push_back(SOS_Cascade<mytype>({{7.27778629e-04, 1.45555726e-03, 7.27778629e-04, 1., 0., 0.}, {1., -2., 1., 1., 0., 0.}}));
    stage0.push_back(SOS_Cascade<mytype>({{0.59180756, -1.18361513, 0.59180756, 1., 0., 0.}, {1., -2., 1., 1., 0., 0.}}));
    stage0.push_back(SOS_Cascade<mytype>({{0.07132238, 0.14264477, 0.07132238, 1., 0., 0.}, {1., -2., 1., 1., 0., 0.}}));
    stage0.push_back(SOS_Cascade<mytype>({{0.10221065, -0.20442131, 0.10221065, 1., 0., 0.}, {1., -2., 1., 1., 0., 0.}}));

    std::vector<SOS_Cascade<mytype>> stage1;
    stage1.push_back(SOS_Cascade<mytype>({{2.35771404e-07, 4.71542809e-07, 2.35771404e-07, 1.00000000e+00, -1.86926994e+00, 8.77305980e-01},
        {1.00000000e+00, 2.00000000e+00, 1.00000000e+00, 1.00000000e+00, -1.96727793e+00, 9.67804744e-01},
        {1.00000000e+00, -2.13069326e+00, 1.13985317e+00, 1.00000000e+00, -1.96727793e+00, 9.67804744e-01}}));
    stage1.push_back(SOS_Cascade<mytype>({{0.90649021, -1.84264253, 0.93664576, 1., -1.86926994, 0.87730598},
        {1., -2., 1., 1., -1.86926994, 0.87730598},
        {1., -2., 1., 1., -1.96727793, 0.96780474}}));
    stage1.push_back(SOS_Cascade<mytype>({{1.36351063e-03, -8.27245890e-04, 7.85735468e-03, 1.00000000e+00, -1.05283002e-01, 1.73533038e-01},
        {1.00000000e+00, 2.00000000e+00, 1.00000000e+00, 1.00000000e+00, -1.48462585e+00, 5.92535342e-01},
        {1.00000000e+00, 2.00000000e+00, 1.00000000e+00, 1.00000000e+00, -1.48462585e+00, 5.92535342e-01}}));
    stage1.push_back(SOS_Cascade<mytype>({{0.2051346, -0.51397462, 0.34619808, 1., -0.105283, 0.17353304},
        {1., -2., 1., 1., -0.105283, 0.17353304},
        {1., -2., 1., 1., -1.48462585, 0.59253534}}));

    std::vector<SOS_Cascade<mytype>> stage2;
    stage2.push_back(SOS_Cascade<mytype>({{2.08020567e-06, -1.26206687e-06, 1.19873754e-05, 1.00000000e+00, -1.05283002e-01, 1.73533038e-01},
        {1.00000000e+00, 2.00000000e+00, 1.00000000e+00, 1.00000000e+00, -9.99488553e-01, 3.54055437e-01},
        {1.00000000e+00, 2.00000000e+00, 1.00000000e+00, 1.00000000e+00, -1.48462585e+00, 5.92535342e-01},
        {1.00000000e+00, -2.82297191e+00, 2.82441645e+00, 1.00000000e+00, -1.93457240e+00, 9.36646028e-01},
        {1.00000000e+00, -2.50554819e+00, 1.68766305e+00, 1.00000000e+00, -1.93457240e+00, 9.36646028e-01}}));
    stage2.push_back(SOS_Cascade<mytype>({{0.61205074, -1.30409238, 0.69764797, 1., -0.99948855, 0.35405544},
        {1., -2.0654253, 1.06763918, 1., -0.99948855, 0.35405544},
        {1., -2.03272193, 1.03326627, 1., -1.86926994, 0.87730598},
        {1., -2., 1., 1., -1.9345724, 0.93664603},
        {1., -2., 1., 1., -1.96727793, 0.96780474}}));

    SOS_Cascade<mytype> stage3({{1., 0., 0., 1., -1.73943503, 0.76967138}, {1., 0., 0., 1., -1.73943503, 0.76967138}});

    std::vector<int> numPaths{10000, 50000, 100000};
    std::vector<int> irLen{fs};
    std::vector<int> numChannels{4};
    int numBands = stage0.size();

    for (auto nPaths : numPaths)
    {
        for (auto iLen : irLen)
        {
            for (auto nChan : numChannels)
            {
                std::cout << "Number of Paths: " << nPaths << std::endl;
                std::cout << "Length of IR: " << iLen << std::endl;
                std::cout << "Number of channels: " << nChan << std::endl;

                double totalTimeElapsed_us = 0;
                double impTrainGenTime_us = 0;
                for (int trial = 0; trial < numtrials; ++trial)
                {
                    // clear filter states
                    for (auto& s : stage0)
                        s.clearStates();
                    for (auto& s : stage1)
                        s.clearStates();
                    for (auto& s : stage2)
                        s.clearStates();
                    stage3.clearStates();

                    std::vector<int> pathArrival(nPaths, 0);
                    std::vector<std::vector<float>> pathDirAmp(nPaths, std::vector<float>(nChan, 0));
                    std::vector<std::vector<float>> pathReflAmp(nPaths, std::vector<float>(numBands, 1));

                    // Generate random paths
                    for (int i = 0; i < nPaths; ++i)
                    {
                        pathArrival[i] = rand() % iLen;
                        for (int j = 0; j < nChan; ++j)
                            pathDirAmp[i][j] = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                        for (int j = 0; j < numBands; ++j)
                            pathReflAmp[i][j] = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                    }

                    auto start = std::chrono::high_resolution_clock::now();

                    for (int chan = 0; chan < nChan; ++chan)
                    {
                        // Generate impulse train
                        auto startImp = std::chrono::high_resolution_clock::now();
                        std::vector<std::vector<float>> iTrainBands(numBands, std::vector<float>(iLen, 0));
                        for (int band = 0; band < numBands; ++band)
                        {
                            for (int path = 0; path < nPaths; ++path)
                                iTrainBands[band][pathArrival[path]] = pathDirAmp[path][chan] * pathReflAmp[path][band];
                        }
                        auto stopImp = std::chrono::high_resolution_clock::now();
                        auto elapsedImp = std::chrono::duration_cast<ChronoResolution>(stopImp - startImp);
                        impTrainGenTime_us += static_cast<double>(elapsedImp.count());

                        // filter stage 0
                        for (int band = 0; band < numBands; ++band)
                            stage0[band].processBlock(iTrainBands[band]);

                        // filter stage 1
                        for (int band = 0; band < numBands / 2; ++band)
                        {
                            // add successive band inputs
                            std::transform(iTrainBands[2 * band].begin(),
                                iTrainBands[2 * band].end(),
                                iTrainBands[2 * band + 1].begin(),
                                iTrainBands[2 * band].begin(),
                                std::plus<mytype>());
                            stage1[band].processBlock(iTrainBands[2 * band]);
                        }

                        // filter stage 2
                        for (int band = 0; band < numBands / 4; ++band)
                        {
                            // add successive band inputs
                            std::transform(iTrainBands[4 * band].begin(),
                                iTrainBands[4 * band].end(),
                                iTrainBands[4 * band + 2].begin(),
                                iTrainBands[4 * band].begin(),
                                std::plus<mytype>());
                            stage2[band].processBlock(iTrainBands[4 * band]);
                        }

                        // filter stage 3
                        // add successive band inputs
                        std::transform(iTrainBands[0].begin(), iTrainBands[0].end(), iTrainBands[4].begin(), iTrainBands[0].begin(), std::plus<mytype>());
                        stage3.processBlock(iTrainBands[0]);
                    }

                    auto stop = std::chrono::high_resolution_clock::now();
                    auto elapsed = std::chrono::duration_cast<ChronoResolution>(stop - start);
                    totalTimeElapsed_us += static_cast<double>(elapsed.count());
                }
                std::cout << "\tTotal time elapsed: " << totalTimeElapsed_us << " milliseconds" << std::endl;
                std::cout << "\tAverage time per trial: " << totalTimeElapsed_us / numtrials << " milliseconds" << std::endl;
                std::cout << "\tTime per path (impulse train generation): " << impTrainGenTime_us / numtrials << " milliseconds" << std::endl;
            }
        }
    }
    return 0;
}