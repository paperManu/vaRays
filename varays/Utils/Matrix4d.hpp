// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_MATRIX4D__
#define __VARAYS_MATRIX4D__

#include <vector>

namespace varays
{
template <class varType> class Matrix4d
{
  public:
    Matrix4d(size_t wsize, size_t xsize, size_t ysize, size_t zsize, varType const& defaultValue = {})
    {
        matrix = {wsize, {xsize, {ysize, std::vector<varType>(zsize, defaultValue)}}};
    };
    varType& operator()(size_t w, size_t x, size_t y, size_t z)
    {
        assert(w < matrix.size());
        assert(x < matrix[w].size());
        assert(y < matrix[w][x].size());
        assert(z < matrix[w][x][y].size());
        return matrix[w][x][y][z];
    }

  private:
    std::vector<std::vector<std::vector<std::vector<varType>>>> matrix;
};
} // namespace varays
#endif