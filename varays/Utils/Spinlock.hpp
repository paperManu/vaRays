// Copyright (C) 2013 Emmanuel Durand
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the
// Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
// Boston, MA 02110-1301, USA.

#ifndef VARAYS_SPINLOCK_H
#define VARAYS_SPINLOCK_H

#include <atomic>
#include <thread>

namespace varays
{

/*************/
class Spinlock
{
  public:
    void unlock() { _lock.clear(std::memory_order_release); }
    bool try_lock() { return !_lock.test_and_set(std::memory_order_acquire); }
    void lock()
    {
        for (uint32_t i = 0; !try_lock(); ++i)
            if (i % 100 == 0)
                std::this_thread::yield();
    }

  private:
    std::atomic_flag _lock = ATOMIC_FLAG_INIT;
};
}; // namespace varays

#endif // VARAYS_SPINLOCK_H
