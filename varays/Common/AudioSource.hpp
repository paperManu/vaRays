// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <string>
#include <vector>

#include <Eigen/Dense>

#include "./Energy.hpp"
#include "./SoundEvent.hpp"

#ifndef __VARAYS_AUDIOSOURCE__
#define __VARAYS_AUDIOSOURCE__

namespace varays
{
class AudioSource
{
  public:
    AudioSource(unsigned int id,
        const std::string& name,
        const Eigen::Vector3f& sourcePos,
        const std::vector<float>& frequencies,
        float energyFromRays);

    // Should be called every time the source changes positions, the listener changes positions or the frequencies change
    unsigned int getId() const { return id_; }
    std::string getName() const { return name_; }

    void setPos(const Eigen::Vector3f& sourcePos);    
    Eigen::Vector3f getPos() const { return position_; }

    void setEnergy(float energy_);
    varays::energy getEnergy() const {return energy_; };
    
    float getRadius() const { return radius_; }

  private:
    unsigned int id_;
    std::string name_;
    Eigen::Vector3f position_;
    float radius_ = 3.f;
    varays::energy energy_;
};
}; // namespace varays
#endif
