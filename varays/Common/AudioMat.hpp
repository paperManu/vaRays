// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <map>

#ifndef __VARAYS_AUDIOMAT__
#define __VARAYS_AUDIOMAT__

namespace varays
{
using materialProperty = std::map<float /*frequency*/, float /*coefficient*/>;

class AudioMat
{
  public:
    AudioMat(const materialProperty& absorption = {}, const materialProperty& transmission = {}, const materialProperty& scattering = {})
         : absorption_(absorption)
         , transmission_(transmission)
         , scattering_(scattering){};

    float getAbsorption(float frequency);
    float getTransmission(float frequency);
    float getScattering(float frequency);
    bool ChangeProperty(const std::string& property, float frequency, float value);

  private:
    materialProperty absorption_;
    materialProperty transmission_;
    materialProperty scattering_;
};

} // namespace varays
#endif