//  Copyright (C) 2019- [SAT] Metalab (Harish Venkatesan)
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __RAYCONVOLVER__
#define __RAYCONVOLVER__

#include <memory>
#include <string.h>
#include <vector>

#include <jack/jack.h>

#include "../Common/SoundEvent.hpp"
#include "../Utils/Logger.hpp"
#include "./DopplerEngine/DopplerEngine.hpp"
#include "./jclient.hpp"
#include "./zita-convolver/zita-convolver.hpp"

namespace varays
{
class VaraysAuralizer
{
  public:
    enum ERROR_CODE
    {
        NOERR,
        ERR_OTHER,
        ERR_SYNTAX,
        ERR_PARAM,
        ERR_ALLOC,
        ERR_CANTCD,
        ERR_COMMAND,
        ERR_INCONV,
        ERR_NOCONV,
        ERR_IONUM
    };

    /**
     * Convolver and doppler engine parameters
     */
    struct VaraysAuralizerParams
    {
        std::string jackServerName_; /** Name of the Jack server to connect to*/
        unsigned int numInputs_;
        unsigned int numOutputs_;

        unsigned int convSize_; /** Lenght of the IR being convolved*/
        unsigned int convPartitionSize_; /** Size of the smallest convolution partition*/
        float convDensity_;

        bool enableDoppler_;
        unsigned int dopplerInterpOrder_; /** Lagrange interpolation order*/
        float dopplerMaxVelocity_; /** Maximum allowed relative velocity between the sources and the listener (m/s)*/
        float dopplerSpeedOfSound_; /** Speed of sound in air (m/s)*/

        /*
         * VaraysAuralizer settings
         *
         * @param   jackServerName      Name of the Jack server to connect to
         * @param	numInputs			Number of input channels (one for each source in the scene)
         * @param	numOutputs			Number of output channels (corresponding to the ambisonic order)
         * @param	convSize			Size of the impulse response
         * @param	convPartitionSize	Size of the smallest partition in the convolver
         * @param	enableDoppler		Enable doppler shifter
         * @param	dopplerInterpOrder	Order of the Lagrange interpolation filter in the fractional delay line
         * @param	dopplerMaxVelocity	Maximum relative velocity of sources with respect to the listener (m/s)
         * @param	dopplerSpeedOfSound	Speed of sound in air (m/s)
         */
        VaraysAuralizerParams(const std::string& jackServerName = "default",
            unsigned int numInputs = 1,
            unsigned int numOutputs = 1,
            unsigned int convSize = 104800,
            unsigned int convPartitionSize = 256,
            bool enableDoppler = true,
            unsigned int dopplerInterpOrder = 1,
            float dopplerMaxVelocity = 50.0f,
            float dopplerSpeedOfSound = 343.0f)
            : jackServerName_(jackServerName)
            , numInputs_(numInputs)
            , numOutputs_(numOutputs)
            , convSize_(convSize)
            , convPartitionSize_(convPartitionSize)
            , convDensity_(1.0f)
            , enableDoppler_(enableDoppler)
            , dopplerInterpOrder_(dopplerInterpOrder)
            , dopplerMaxVelocity_(dopplerMaxVelocity)
            , dopplerSpeedOfSound_(dopplerSpeedOfSound)
        {
        }
    };

    static const uint32_t kMaxSize_ = 0x00100000; /** Maximum allowed length of the convolved IR*/

    /**
     * VaraysAuralizer constructor
     *
     * @param   params  `VaraysAuralizerParams` object with set parameters
     */
    VaraysAuralizer(VaraysAuralizerParams params = VaraysAuralizerParams());
    ~VaraysAuralizer();

    /**
     * Update the impulse response for a given source.
     *
     * @param   sourceName  Name of the source
     * @param   nchan       Number of channels
     * @param   data        Multichannel interleaved impulse response
     * @return Error code
     */
    ERROR_CODE updateIR(const std::string& sourceName, uint32_t nchan, const std::vector<float>& data);

    /**
     * Update distance and direction of arrival of the direct sound for a given source.
     *
     * This function updates parameters required to produce Doppler effect.
     *
     * @param   sourceName          Name of the source
     * @param   directSoundEvent    `varays::SoundEvent` object representing the direct sound
     * @param   sourceInsight       Boolean that says if a direct path exists between the source and the listener
     * @param   teleport            Boolean to indicate if the source has moved instantaneously. True: No Doppler effect, False: Produce Doppler effect.
     * @return  Error code
     */
    ERROR_CODE updateDelayLines(const std::string& sourceName, const SoundEvent& directSoundEvent, bool sourceInsight = true, bool teleport = false);

    /**
     * Create jack input and resources for a new source
     *
     * @param   sourceName  Name of the source
     * @return  Error code
     */
    ERROR_CODE newSource(const std::string& sourceName);

    /**
     * Delete jack input and existing resources of an existing source
     *
     * @param   sourceName  Name of the source
     * @return  Error code
     */
    ERROR_CODE deleteSource(const std::string& sourceName);

    /**
     * Enable/Disable SIMD vectorized operations in the convolver
     */
    void setVectorizedOperations(bool enableVectorization);

    /**
     * Return JClient related status flags
     */
    unsigned int getJClientFlags();

    /**
     * Return the sample rate of the jack client
     */
    unsigned int getSampleRate() { return fsamp_; }

    /**
     * Check if Doppler engine is enabled
     */
    bool dopplerEnabled() { return (dopplerEngine_.get() != nullptr); }

    /**
     * Check if resources have been correctly allocated
     */
    bool isAllocated() { return allocOkay_; }

    /**
     * Return the name of the Jack client
     */
    std::string getJackClientName() { return jackname_; }

    /**
     * Return the name of the Jack server to which the Jack client is attached
     */
    std::string getJackServerName() { return jackserv_; }

#ifdef __CONV_TEST__
    // These functions are used to expose the Jack client for testing
    jack_client_t* getJackClientPtr() { return jclient_->getJackClient(); }
    Jclient* getJclient() { return jclient_.get(); }
#endif

  private:
    /**
     * Start the Jack client
     */
    ERROR_CODE start_jclient();

    /**
     * Check if an input-output connection exists
     */
    ERROR_CODE check_inout(unsigned int ip, unsigned int op);

    /**
     * Create ambisonic output ports. This function is called before starting the Jack client.
     */
    void makeports();

    /**
     * Print error log corresponding to the given error code
     *
     * @param   err Error code
     */
    void logError(ERROR_CODE err);

    // J/Zita Convolver related variables
    Convproc convproc_{};
    std::unique_ptr<DopplerEngine> dopplerEngine_{nullptr};
    std::unique_ptr<Jclient> jclient_{};
    std::string jackname_{"vaRays Auralizer"};
    std::string jackserv_{"default"};
    unsigned int latency_{0};
    unsigned int options_{0};
    unsigned int fsamp_{0}; /** Jack session sample rate*/
    unsigned int fragm_{0}; /** Jack session buffer size*/
    unsigned int ninp_{0};
    unsigned int nout_{0};
    unsigned int size_{0}; /** Maximum length of the convolved IR*/

    bool allocOkay_{false}; // used to check success of constructor via bool idiom

    Logger logger_{"VaraysAuralizer"};

    // JClient i/o port names
    std::vector<std::string> inp_name_{static_cast<size_t>(Convproc::MAXINP)};
    std::vector<std::string> out_name_{static_cast<size_t>(Convproc::MAXOUT)};
    std::vector<std::string> inp_conn_{static_cast<size_t>(Convproc::MAXINP)};
    std::vector<std::string> out_conn_{static_cast<size_t>(Convproc::MAXOUT)};

};
}; // namespace varays
#endif //__VARAYSCONVOLVER__